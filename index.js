const slides = document.getElementsByClassName('carousel-item');
const indicators = document.getElementsByClassName("progress-indicator")
let slidePosition = 0;
const totalSlides = slides.length;

document.getElementById('carousel-button-next').addEventListener('click', moveToNextSlide);
document.getElementById('carousel-button-prev').addEventListener('click', moveToPrevSlide);


function moveToNextSlide() {
    slides[slidePosition].classList.remove('carousel-item-visible')
    indicators[slidePosition].classList.remove("selected")
    
    if (slidePosition === totalSlides - 1) {
        slidePosition = 0;
    } else {
        slidePosition++;
    }
    indicators[slidePosition].classList.add("selected")
    slides[slidePosition].classList.add("carousel-item-visible");
}

function moveToPrevSlide() {
    slides[slidePosition].classList.remove('carousel-item-visible')
    indicators[slidePosition].classList.remove("selected")
    if (slidePosition === 0) {
        slidePosition = totalSlides - 1;
    } else {
        slidePosition--;
    }
    indicators[slidePosition].classList.add("selected")
    slides[slidePosition].classList.add("carousel-item-visible");
}

setInterval(function() {
   moveToNextSlide()
  },5000);